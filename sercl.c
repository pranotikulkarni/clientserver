#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <math.h>
#include <stdio.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h> //inet_addr and the struct sockaddr_in
#include <unistd.h>   //exit and size_t
#include <pthread.h> // for threading specifically for pthread

#define BACKLOG 10    //used backlog for storing the 10 client threads which have been called later

// the showhandlestatus function prototype
void *show_handlestatus(void *);

int main() {
	int socket_desc, client_sock, c;
	struct sockaddr_in server, client;
        // Create socket
	socket_desc = socket(AF_INET, SOCK_STREAM, 0);
	if(socket_desc < 0) {
		perror("Socket creation has failed\n");
                exit(1);
	}
	printf("The Socket is created\n");

	// Prepare the sockaddr_in structure
	server.sin_family = AF_INET;
	server.sin_addr.s_addr= INADDR_ANY;
	server.sin_port = htons(8080);

        // Bind that attaches the local machine address to the socket
	if(bind(socket_desc,(struct sockaddr *)&server, sizeof(server)) < 0) {
		// print the error message
		perror("Bind failed... Error...\n");
                exit(1);
	}
	puts("Bind done successfully\n");

        // Listen to accept my client connection(10 threads are in this case)
	listen(socket_desc, BACKLOG);

	// Accept the incoming connection from all the client threads
	puts("Waiting for incoming connections...");
	c = sizeof(client);
        while((client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c)))
         {
	   puts("Connection accepted");
           pthread_t thread_id;
           //After the connection is established i have used the clock_t function to show start
           // and end time i.e after one client is disconnected
           clock_t begin = clock();
           printf("start time: %e\n", (double)(begin)/CLOCKS_PER_SEC*1000);
	     if(pthread_create( &thread_id, NULL,  show_handlestatus, (void*) &client_sock) < 0)
               {
		 perror("could not create thread");
	       }
		 puts("Handler assigned"); //Handler is assigned to show that the client requests are been taken care of by server
         }
         if (client_sock < 0)
           {
		puts("accept failed");
		exit (1);
	   }
     return 0;
}

// This will handle requets made by each client
void *show_handlestatus(void *socket_desc)
{
	// Get the socket descriptor
	int sock = *(int*)socket_desc;
        struct sockaddr_in client;
        char sendbuff[100],client_message[2000];
	int a,b,c;
        long int begin;
	// Send some message to client
	char message[50] = "Greetings!\n";
	a = write(sock, message, strlen(message));
        c=sizeof(client);
      while(b=recv(sock,client_message,2000,0)>0 && b==1)
       {
         send(sock,client_message,b,0);   //if the client sends a message then only the server will send a reply
       }
      //getpeernamme is to show the ip address in this case it is the loopback address
      //this is later called in pthread_create to show the ip address after every client.
      if(getpeername( sock, (struct sockaddr *)&client, &c)==-1){
          perror("failed");
         }
         printf("%s\n",inet_ntoa(client.sin_addr));
      if(b == 0)
       {
        puts("Client disconnected");
        clock_t end = clock();
        printf("end time: %e\n", (double)(end)/CLOCKS_PER_SEC*1000);
       }
	return 0;
}
