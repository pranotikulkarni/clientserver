Client-server application handling multiple clients on single server using TCP socket and Linux pthreads.
1) This application i have declared 10 clients using pthread function. Each time you start the client process, one at a time the client thread is invoked and you can see the Client connection, when its disconnected and also the start and end time of client.
2) I have also fetched the ip address for each client thread.
3) You can run the client process on multiple command applications to see how the client is connected and also when you send a message to server it replies back instantly.
4) The multithreading part has been successfully implemented using posix threads. I thought of using forks, but forks can create some issues so didn't use them.
5) Have commented each line of code. Used proper function names, variable and constant names.

Running this application:
1) Type the gcc server.c -o server -lpthread command to first compile the server program.
Run it using: ./server
2) Similar way for the client program using new cmd window to observe all the communication.