#include <stdio.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <unistd.h>
#include <pthread.h>

#define MAX_SIZE 50
#define NUM_CLIENT 10
void *show_handler(void *socket_desc);
int main()
{
    int socket_desc ,i;
    pthread_t s_thread;
    //running 10 client threads in for loop.After each client there is a 10 secs sleep time 
    //after which the next one is invoked once you get reply from server
    for (i=1; i<=NUM_CLIENT; i++) {
    if( pthread_create( &s_thread , NULL ,  show_handler , (void*)(intptr_t)i) < 0)
        {
            perror("could not create thread");
            return 1;
        }
    sleep(10);
    printf("It will wait for 10 secs to run the next client\n\n");
}
    pthread_exit(NULL);
    return 0;
}

void *show_handler(void *threadid)
{
    int threadnum = (intptr_t)threadid;
    int sock_desc;
    char message[2000],server_reply[2000];
    struct sockaddr_in serv_addr;
    char sbuff[MAX_SIZE],rbuff[MAX_SIZE];

    if((sock_desc = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        printf("Failed creating socket\n");

    bzero((char *) &serv_addr, sizeof (serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;  //should accept any ip address but in my case its taking loopback ip(127.0.0.1)
    serv_addr.sin_port = htons(8080);

    if (connect(sock_desc, (struct sockaddr *) &serv_addr, sizeof (serv_addr)) < 0) {
        printf("Failed to connect to server\n");
    }
    printf("\nConnected successfully client:%d\n", threadnum);

    while(1)
    {
        printf("For thread : %d\n", threadnum);
        printf("Enter a message: \n"); //enter a message and press enter to get server reply
        scanf("%s", message);
      if(recv(sock_desc,server_reply,2000,0)>0)
       puts(server_reply);
       fgets(sbuff, MAX_SIZE , stdin);
       send(sock_desc,sbuff,strlen(sbuff),0);
       if(recv(sock_desc,rbuff,MAX_SIZE,0)==0)
            printf("Error");
       else
           fputs(rbuff,stdout);
        bzero(rbuff,MAX_SIZE);
     }
    close(sock_desc);
    return 0;
}
